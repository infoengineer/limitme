Option Explicit
On Error Resume Next
Dim WshShell, FSO, F, Autorun, Start, Limit, Day, LeftMinutes
Set FSO = CreateObject("Scripting.FileSystemObject")
Set F = FSO.GetFile(Wscript.ScriptFullName)
Set WshShell = CreateObject("WScript.Shell")
Autorun = WshShell.RegRead("HKCU\Software\Microsoft\Windows\CurrentVersion\Run\LimitMe")
If Err.Number <> 0 Then
	WshShell.RegWrite "HKCU\Software\Microsoft\Windows\CurrentVersion\Run\LimitMe", F, "REG_SZ"
End If
Start = Now
Limit = WshShell.RegRead("HKCU\Software\Infoengineer\LimitMe\Limit")
If Err.Number <> 0 Then
    WshShell.RegWrite "HKCU\Software\Infoengineer\LimitMe\Limit", "abc", "REG_SZ"
    Limit = WshShell.RegRead("HKCU\Software\Infoengineer\LimitMe\Limit")
End If
Day = WshShell.RegRead("HKCU\Software\Infoengineer\LimitMe\Day")
If Err.Number <> 0 Then
    WshShell.RegWrite "HKCU\Software\Infoengineer\LimitMe\Day", DatePart("d", Start), "REG_DWORD"
    Day = WshShell.RegRead("HKCU\Software\Infoengineer\LimitMe\Day")
End If
LeftMinutes = WshShell.RegRead("HKCU\Software\Infoengineer\LimitMe\LeftMinutes")
If Err.Number <> 0 Then
    WshShell.RegWrite "HKCU\Software\Infoengineer\LimitMe\LeftMinutes", Len(Limit) * 60, "REG_DWORD"
    LeftMinutes = WshShell.RegRead("HKCU\Software\Infoengineer\LimitMe\LeftMinutes")
End If
If DatePart("d", Start) <> Day Then
    WshShell.RegWrite "HKCU\Software\Infoengineer\LimitMe\LeftMinutes", Len(Limit) * 60, "REG_DWORD"
    LeftMinutes = WshShell.RegRead("HKCU\Software\Infoengineer\LimitMe\LeftMinutes")
End If
If LeftMinutes = 0 Then
	WshShell.Run "shutdown /s"
Else
	WshShell.Run "shutdown /s /f /t " & CStr(LeftMinutes * 60)
End If
WshShell.RegWrite "HKCU\Software\Infoengineer\LimitMe\Day", DatePart("d", Start), "REG_DWORD"
Do
	WScript.Sleep 60000
	LeftMinutes = LeftMinutes - 1
	WshShell.RegWrite "HKCU\Software\Infoengineer\LimitMe\LeftMinutes", LeftMinutes, "REG_DWORD" 
Loop Until LeftMinutes = 0